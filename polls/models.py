from django.db import models

# Create your models here.

class Cabecera(models.Model):
    region = models.TextField()
    provincia = models.TextField()
    fecha = models.CharField(max_length=10)
    distrito = models.TextField()
    comunidad = models.TextField()
    sexo = models.TextField()
    direccion = models.TextField()
    nomfamilia = models.TextField()
    sectorintervencion = models.TextField()
    nrointegrantes = models.IntegerField()

    def __str__(self):
        return self.region
