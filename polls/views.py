from django.shortcuts import render
from django.contrib.auth.models import User
from rest_framework import serializers
from tutorial.quickstart.serializers import UserSerializer



# Create your views here.

class UserSerializer(serializers.ModelSerializer):
    queryset = User.objects.all().order_by('-date_joined')

    serializer_class = UserSerializer

    # class Meta:
    #     model = User
    #     fields = ('username', 'email')
